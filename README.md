# OpenML dataset: veteran

https://www.openml.org/d/497

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Veteran's Administration Lung Cancer Trial
Taken from Kalbfleisch and Prentice, pages 223-224

Variables
Treatment  1=standard,  2=test
Celltype   1=squamous,  2=smallcell,  3=adeno,  4=large
Survival in days
Status     1=dead, 0=censored
Karnofsky score
Months from Diagnosis
Age in years
Prior therapy  0=no, 10=yes



Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/497) of an [OpenML dataset](https://www.openml.org/d/497). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/497/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/497/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/497/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

